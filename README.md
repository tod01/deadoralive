# DeadOrAlive project

# DESCRIPTION

The program, DeadOrAlive, allows to model the behavior of a rectangular grid of cells, of configurable length and width. A cell can be in a living or dead state. This cell grid evolves in time. From time t, the next state at t+1 is calculated according to the following rules:
- Any living cell with less than 2 living neighbors dies at the next instant, per subpopulation
- Any living cell with exactly 2 or 3 living neighbors continues to live at the next instant
- Any cell with more than 3 living neighbors dies by overpopulation
- And any dead cell with exactly 3 living neighbors comes to life!

# SETUP

## Run the following scripts from the command line (linux)
* Create a virtual environment : python3 -m venv my_env
* Activate the environment: source my_env/bin/activate
* Install the dependence of the project from the root: pip install -r requirements.txt
* Add path of the created env `my_env`: `export PYTHONPATH=$PYTHONPATH: absolute_path_of_the_env`

# PROGRAM EXECUTION

        Syntax: python3 src/main.py -f file_name | -s string_value

**It is important to be in the `deaoralive` folder before launching the script.**

The program takes as input a string or a file of the following form:

```
x_max y_max number_of_iterations
x_cell_1 y_cell_1
x_cell_2 y_cell_2
...
x_cell_n y_cell_n
```
For example, the file or the string can be:
```
3 4 3
0 0
1 0
2 0
```

For example, we have a file named `sample_data.txt` to execute it we do : 
```
    python3 src/main.py -f sample_data.txt
```

We obtained the following result after running the program:

```
                ***** State 0 *****
###
...
...
...

                ***** State 1 *****
.#.
.#.
...
...

                ***** State 2 *****
...
...
...
...

                ***** State 3 *****
...
...
...


```

* *State 0* represents the initial state of the file

* **The get some help for the syntax launch** : ``` python3 src/main.py -h ```

* **'.' represents a dead cell and '#' represents a living cell**

# TEST

**To test the program, run the following command:**
```
                        python -m unittest discover
```

**We got as a result**

```
.....
----------------------------------------------------------------------
Ran 5 tests in 0.002s

OK
```

All 5 tests passed!

# Next Step

1. Dockerize the project.
2. Add a server to visualize the result through an html page.