import os
from typing import List, Dict

import numpy
import numpy as np


def instantiate_array(nb_column: int, nb_row: int) -> numpy.array:

    """
        Create a new array filled with 0.
    :param nb_column: Number of columns of the array
    :param nb_row: Number of rows of the array
    :return: Return a numpy array
    """
    # check the input values
    assert (nb_column > 0 and nb_row > 0), "the number of columns and the number of rows must be greater than 0 !"

    # initialize the array
    tab = np.zeros((nb_row, nb_column), dtype=int)

    return tab


def construct_string_data(data: str) -> Dict:

    """
        Get the data from the string `data` and construct the array and its arguments.
    param data: the file name
    :return: Return a dictionary
    """

    str_data = data.split('\n')
    # get the first line that corresponds to the arguments (number of rows, columns and iterations)
    args = str_data[0].split(' ')
    game_parameters = construct_array(args)
    array = game_parameters["array"]

    # construct the array
    for line in str_data[1:]:
        alive_position = line.split(' ')

        assert len(alive_position) == 2, "2 values must be filled in for the cell i,j"
        y_alive = int(alive_position[1])
        x_alive = int(alive_position[0])
        array[y_alive, x_alive] = 1

    return game_parameters


def file_exist(file_name: str) -> bool:

    """
        Check if the file exists
    :param file_name: the name of the file
    :return: bool
    """

    return os.path.exists(file_name)


def construct_file_data(file_name: str) -> Dict:

    """
        Get the data from the file and construct the array and its arguments.
    :param file_name: the file name
    :return: Return a dictionary
    """

    # be sure that the file exists
    assert file_exist(file_name) is True, f"{file_name} doesn't exist"

    game_parameters = None

    # open the file and construct the array
    with open(file_name) as f:

        # get the first line that corresponds to the arguments (number of rows, columns and iterations)
        args = f.readline().split(' ')
        game_parameters = construct_array(args)
        array = game_parameters["array"]

        for line in f.readlines():
            alive_position = line.split(' ')
            assert len(alive_position) == 2, "2 values must be filled in for the cell i,j"
            y_alive = int(alive_position[1])
            x_alive = int(alive_position[0])
            array[y_alive, x_alive] = 1

    return game_parameters


def construct_array(args: List[str]) -> Dict:

    """
        Construct an array
    :param args: a list containing the number of rows, columns and iterations needed for `deadOrAlive` program
    :return: Dict
    """

    assert len(args) == 3, "Arguments list must be equal to 3"

    try:

        x_cells_alive = int(args[0])
        y_cells_alive = int(args[1])
        nb_of_iterations = int(args[2])

        # instantiate the array
        array = instantiate_array(x_cells_alive, y_cells_alive)

        return {
            "x_cells_alive": x_cells_alive,
            "y_cells_alive": y_cells_alive,
            "nb_iterations": nb_of_iterations,
            "array": array
        }
    except Exception as error:
        raise Exception(f"Construction error: {error}")
