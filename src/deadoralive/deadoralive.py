from typing import Dict

import numpy as np
from src.utils import helpers


class DeadOrAlive:
    """
        Implement the DeadOrAlive program
    """

    # the number of parameters used in the program
    NB_PARAMETERS: int = 4

    def __init__(self, parameters: Dict):

        """
            Create the default array and its parameters
        :param parameters: A dictionary that contains the information of the program
        """

        # make sure that the parameter arguments contain all the necessary values
        assert len(parameters) == self.NB_PARAMETERS, f"{self.NB_PARAMETERS} are expected !"

        assert ("array" in parameters and "x_cells_alive" in parameters and "nb_iterations" in parameters
                and "y_cells_alive" in parameters), f"Parameters {parameters.keys()} expected ! "

        self.init_array = parameters["array"]
        self.nb_columns = parameters["x_cells_alive"]
        self.nb_rows = parameters["y_cells_alive"]
        self.nb_iterations = parameters["nb_iterations"]
        self.current_state = self.init_array  # current state is the default one

    def next_state(self) -> np.array:
        """
            Calculate the next state of the array.
        :return: Return a new numpy array
        """

        # create a new numpy array
        new_array = helpers.instantiate_array(self.nb_columns, self.nb_rows)

        for row, column in np.ndindex(self.current_state.shape):

            # calcuate the number of living neighbors
            row_column_1 = self.current_state[row, column - 1] if column > 0 else 0
            row_1_column = self.current_state[row - 1, column] if row > 0 else 0
            next_row_column = self.current_state[row + 1, column] if row + 1 < self.nb_rows else 0
            row_next_column = self.current_state[row, column + 1] if column + 1 < self.nb_columns else 0

            row_1_column_1 = self.current_state[row - 1, column - 1] if (column > 0 and row > 0) else 0
            row_1_next_column = self.current_state[row - 1, column + 1] if (
                    row > 0 and column + 1 < self.nb_columns) else 0
            next_row_column_1 = self.current_state[row + 1, column - 1] if (
                    row + 1 < self.nb_rows and column > 0) else 0
            next_row_next_column = self.current_state[row + 1, column + 1] if (
                    row + 1 < self.nb_rows and column + 1 < self.nb_columns) else 0

            # sum of all neighbor values
            num_alive = sum([row_column_1, row_1_column, next_row_column, row_next_column, row_1_column_1,
                             row_1_next_column, next_row_column_1, next_row_next_column])

            # update the value of the cell according to the number of living neighbors
            if self.current_state[row, column] == 1 and num_alive < 2 or num_alive > 3:
                new_array[row, column] = 0
            if (self.current_state[row, column] == 1 and 2 <= num_alive <= 3) or (
                    self.current_state[row, column] == 0 and num_alive == 3):
                new_array[row, column] = 1

        # update the current state
        self.current_state = new_array

        return new_array

    def display_current_state(self, state: int):
        """
            Display an array.
            '.' represent dead cell and '#' represent a living cell
        :param state: The current state
        """

        print(f"\t\t***** State {state} *****")

        for row in range(self.nb_rows):
            for column in range(self.nb_columns):
                print("#" if self.current_state[row][column] else ".", end='')
            print("")

        print("")

    def run_states(self, display=True):
        """
            Run the states of the program
        :param display: Display the cells in each step of the running

        """

        # if we need to display the current state
        if display:
            self.display_current_state(0)

        for t in range(self.nb_iterations):
            self.next_state()
            if display:
                self.display_current_state(t + 1)
