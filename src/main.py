import argparse
import getopt
from deadoralive import deadoralive as dol
from utils import helpers


if __name__ == '__main__':

    try:

        # Initialize parser
        parser = argparse.ArgumentParser("DeadOrAlive program.")

        # Adding optional argument
        parser.add_argument("-f", "--file", type=str, help='FILE is the name of the file containing the parameters of '
                                                           'the array')
        parser.add_argument("-s", "--string", type=str, help='String containing the parameters of the array')

        # Read arguments from command line
        args = parser.parse_args()

        # check arguments
        if args.file:
            data = helpers.construct_file_data(args.file)
        elif args.string:
            data = helpers.construct_string_data(args.string)
        else:
            raise getopt.error("Unknown parameters. Add -h parameter for help")

        # deadOrAlive program
        dead_or_alive = dol.DeadOrAlive(data)
        dead_or_alive.run_states()

    except getopt.error as err:
        # output error, and return with an error code
        print(str(err))

