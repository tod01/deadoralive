from typing import Dict
from unittest import TestCase

import numpy as np
from deepdiff import DeepDiff
from src.deadoralive.deadoralive import DeadOrAlive
from src.utils import helpers


class DeadOrLiveTesting(TestCase):
    """
        This class tests the DeadOrLive program
    """

    def test_instantiate_array(self):

        """
            Verify the output of `instantiate_array` function
        :return: bool
        """

        new_array = np.array([[0, 0], [0, 0], [0, 0]])
        nb_columns = 2
        nb_rows = 3

        array = helpers.instantiate_array(nb_columns, nb_rows)

        self.assertTrue(np.array_equal(new_array, array))

    def test_next_state(self):

        """
            Verify the output of `next_state` function.
        """

        example_ini_data: Dict = {
            "x_cells_alive": 4,
            "y_cells_alive": 4,
            "nb_iterations": 1,
            "array": np.array([[0, 0, 0, 0], [0, 1, 1, 0], [0, 0, 1, 0], [0, 0, 0, 0]])
        }

        dead_or_alive = DeadOrAlive(example_ini_data)

        next_step = dead_or_alive.next_state()

        expected_result = [[0, 0, 0, 0], [0, 1, 1, 0], [0, 1, 1, 0], [0, 0, 0, 0]]

        self.assertTrue(np.array_equal(next_step, expected_result))

    def test_construct_file_data(self):

        """
            Test the construction of a given file.
            In order to test it, you need to change this function path variable
        """

        file_path = "./data/small_data_file.txt"

        expected_result = {
            'x_cells_alive': 3,
            'y_cells_alive': 4,
            'nb_iterations': 3,
            'array': np.array([[1, 1, 1], [0, 0, 0], [0, 0, 0], [0, 0, 0]])
        }

        result = helpers.construct_file_data(file_path)

        self.assertDictEqual(DeepDiff(result, expected_result), {})

    def test_construct_string_data(self):

        """
            Test the construction of an array using a string
        """

        # the string data
        str_data = "3 4 3\n0 0\n1 0\n2 0"

        expected_result = {
            'x_cells_alive': 3,
            'y_cells_alive': 4,
            'nb_iterations': 3,
            'array': np.array([[1, 1, 1], [0, 0, 0], [0, 0, 0], [0, 0, 0]])
        }

        # construct the data
        result = helpers.construct_string_data(str_data)

        # check
        self.assertDictEqual(DeepDiff(result, expected_result), {})

    def test_state_after_three_running(self):

        """
            Verify the state of the array after three running of the program.
        """

        example_ini_data: Dict = {
            "x_cells_alive": 5,
            "y_cells_alive": 5,
            "nb_iterations": 3,
            "array": np.array([[0, 0, 0, 0, 0], [0, 0, 0, 0, 0], [0, 1, 1, 1, 0], [0, 0, 0, 0, 0], [0, 0, 0, 0, 0]])
        }
        dead_or_alive = DeadOrAlive(example_ini_data)

        dead_or_alive.run_states(display=False)

        expected_result = [[0, 0, 0, 0, 0], [0, 0, 1, 0, 0], [0, 0, 1, 0, 0], [0, 0, 1, 0, 0], [0, 0, 0, 0, 0]]

        # check
        self.assertTrue(np.array_equal(dead_or_alive.current_state, expected_result))
